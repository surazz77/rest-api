const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const product = require('./router')
const mongoose = require('mongoose');

let dev_db_url = 'mongodb://suraj:suraj463093@ds125385.mlab.com:25385/mydatabase'
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/products', product);


app.listen(4000, () => {
    console.log('Server is up and running on port numner ' + 4000);
});