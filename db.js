const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
    name: {type: String, required: 'enter the name', max: 100},
    price: {type: Number, required: 'enter the price'},
});

module.exports = mongoose.model('Product', ProductSchema);