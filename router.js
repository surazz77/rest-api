const express = require('express');
const router = express.Router();

const product_controller = require('./controller');
const auth_controller  = require('./Auth/authController')

router.post('/create',product_controller.product_create);
router.get('/:id',product_controller.product_details)
router.put('/:id/update',product_controller.product_update)
router.delete('/:id/delete',product_controller.product_delete)
router.post('/register',auth_controller.auth)
router.post('/login',auth_controller.login)
router.get('/me',auth_controller.me)
router.get('/test', product_controller.test)

module.exports = router;